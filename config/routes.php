<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use EAP\Packages\ApiDoc\Controller\{UiController, JsonController};

return static function(RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('eap_api_doc_json', '/api/me')
        ->methods(['GET'])
        ->controller(JsonController::class);

    $routingConfigurator->add('eap_api_doc', '/api/doc')
        ->defaults(['area' => null])
        ->methods(['GET'])
        ->controller(UiController::class);
};