<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use EAP\Packages\ApiDoc\Resolver\SchemaResolver;
use EAP\Packages\HttpMapper\Resolver\ClassConstructorValuesResolver;
use EAP\Packages\ApiDoc\Resolver\{
    RouteAttributeResolver,
    RouteParameterResolver,
    RouteResponseResolver,
    RouteObjectResolver
};
use EAP\Packages\ApiDoc\ApiDocConfigure;
use EAP\Packages\ApiDoc\DTO\Object\SchemaCollection;
use EAP\Packages\ApiDoc\ApiDocBuilder;
use EAP\Packages\ApiDoc\DependencyInjection\Compiler\ResolverChain;
use EAP\Packages\ApiDoc\Resolver\Type\DefaultTypeResolver;
use EAP\Packages\ApiDoc\Resolver\ClassPropertyResolveHandler;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use EAP\Packages\ApiDoc\Resolver\Type\UploadFileTypeResolver;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function(ContainerConfigurator $configurator): void {
    $services = $configurator->services();

    $services->defaults()
        ->public()
        ->autowire()
        ->autoconfigure();

    $services->load('EAP\Packages\ApiDoc\Controller\\', __DIR__ . '/../src/Controller');

    // Configure di for bundle tag
    $services->set(ResolverChain::class);

    $services->set(ApiDocConfigure::class)
        ->arg('$title', '%env(APIDOC_TITLE)%')
        ->arg('$description', '%env(APIDOC_DESCRIPTION)%')
        ->arg('$version', '%env(APIDOC_VERSION)%')
        ->arg('$serverUrl', '%env(APIDOC_SERVER_URL)%');

    // Register general resolvers
    $services->set(ClassConstructorValuesResolver::class);
    $services->set(SchemaResolver::class);
    $services->set(SchemaCollection::class);
    $services->set(RouteAttributeResolver::class);
    $services->set(RouteParameterResolver::class);
    $services->set(RouteResponseResolver::class);
    $services->set(RouteObjectResolver::class);

    // Register property type resolvers
    $services->set(UploadFileTypeResolver::class)
        ->tag('api_doc.resolver');

    $defaultTypeResolver = $services->set(DefaultTypeResolver::class);
    $defaultTypeResolver->tag('api_doc.resolver');

    $services->set(ClassPropertyResolveHandler::class)
        ->args([tagged_iterator('api_doc.resolver')]);

    $services->set(ApiDocBuilder::class);
};
