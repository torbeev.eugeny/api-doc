<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Exception;

class MissedAttributeException extends \RuntimeException
{
    protected $message = 'Missed attribute exception';
}