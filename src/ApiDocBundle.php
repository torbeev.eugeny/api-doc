<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc;

use EAP\Packages\ApiDoc\DependencyInjection\Compiler\ResolverTagCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApiDocBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ResolverTagCompilerPass());
    }
}