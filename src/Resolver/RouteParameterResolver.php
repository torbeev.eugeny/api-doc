<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver;

use EAP\Packages\ApiDoc\DTO\Object\SchemaCollection;
use EAP\Packages\ApiDoc\DTO\Swagger\Content\ContentType;
use EAP\Packages\ApiDoc\DTO\Swagger\Content\RouteContentFormData;
use EAP\Packages\ApiDoc\DTO\Swagger\Content\RouteContentJson;
use EAP\Packages\ApiDoc\DTO\Swagger\Route\RouteParameter;
use EAP\Packages\ApiDoc\DTO\Swagger\Route\RouteRequestBody;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaProperty;
use Symfony\Component\Uid\Uuid;
use ReflectionParameter;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;
use EAP\Packages\ApiDoc\DTO\Object\RouteParameterObject;

final class RouteParameterResolver extends RouteAttributeResolver
{
    /**
     * @param ReflectionParameter $parameter
     * @return RouteParameterObject|null
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function __invoke(ReflectionParameter $parameter, $isQuery): ?RouteParameterObject
    {
        $type = str_replace('?', '', (string)$parameter->getType());
        $routeParameter = $this->resolveBasePathParameters($parameter);

        if ($routeParameter !== null) {
            return new RouteParameterObject([$routeParameter], null, null);
        }

        if (!preg_match('/\\\\/', $type, $matches)) {
            return null;
        }

        /** @var SchemaCollection $schemaCollection */
        $schemaCollection = ($this->schemaResolver)($type, $isQuery);
        $mainSchema = $schemaCollection->getMain();

        if (!$mainSchema->isUsable()) {
            $content = match ((string)$mainSchema->getContentType()) {
                ContentType::MULTI_PART_FORM_DATA => new RouteContentFormData($mainSchema->getSchema()),
                default => new RouteContentJson($mainSchema->getSchema())
            };

            return new RouteParameterObject([], new RouteRequestBody($content), $schemaCollection);
        }

        if (!$mainSchema->isQuery()) {
            $requestBody = new RouteRequestBody(new RouteContentJson($this->getDefinitionName($type)));
            return new RouteParameterObject([], $requestBody, $schemaCollection);
        } else {
            $routeParameters = [];
            $this->resolveSchemaProperties($mainSchema->getSchema()->getProperties(), $routeParameters);
            return new RouteParameterObject($routeParameters, null, $schemaCollection);
        }
    }

    /**
     * @param array $properties
     * @param array $routeParameters
     */
    private function resolveSchemaProperties(array $properties, array &$routeParameters): void
    {
        foreach ($properties as $name => $schemaProperty) {
            if (!$schemaProperty instanceof SchemaProperty) {
                continue;
            }
            $routeParameters[] = new RouteParameter('query', $name, $schemaProperty->isRequired(), null, $schemaProperty);
        }
    }

    /**
     * @param ReflectionParameter $parameter
     * @return RouteParameter|null
     */
    private function resolveBasePathParameters(ReflectionParameter $parameter): ?RouteParameter
    {
        return match ($parameter->getType()->getName()) {
            Uuid::class => RouteParameter::inPathWithUuidType($parameter->getName()),
            'string' => RouteParameter::inPathStringType($parameter->getName()),
            'int' => RouteParameter::inPathIntType($parameter->getName()),
            default => null
        };
    }
}