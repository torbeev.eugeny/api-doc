<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver;

use EAP\Packages\ApiDoc\Resolver\Type\TypePropertyResolver;
use ReflectionProperty;
use Exception;
use Traversable;

final class ClassPropertyResolveHandler
{
    public function __construct(
        /** @var iterable<TypePropertyResolver> */
        private iterable $resolvers
    ) {}

    /**
     * @param ReflectionProperty $property
     * @return object
     * @throws Exception
     */
    public function __invoke(ReflectionProperty $property): object
    {
        $type = $property->getType()->getName();

        $resolver = null;
        foreach ($this->resolvers as $availableResolver) {
            /** @var TypePropertyResolver $resolver */
            if ($availableResolver->supports($type)) {
                $resolver = $availableResolver;
                break;
            }
        }

        if ($resolver == null) {
            throw new Exception('Not registered type ' . $type . ' resolver');
        }

        return ($resolver)($property);
    }

    /**
     * @param ReflectionProperty $property
     * @return bool
     */
    public function isSupported(ReflectionProperty $property): bool
    {
        $type = $property->getType()->getName();
        foreach ($this->resolvers as $availableResolver) {
            /** @var TypePropertyResolver $resolver */
            if ($availableResolver->supports($type)) {
                return true;
            }
        }
        return false;
    }
}