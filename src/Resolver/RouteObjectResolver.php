<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver;

use EAP\Packages\ApiDoc\Annotation\Action;
use EAP\Packages\ApiDoc\DTO\Object\RouteObject;
use JetBrains\PhpStorm\Pure;
use ReflectionMethod;
use Symfony\Component\Routing\Annotation\Route;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

class RouteObjectResolver extends RouteAttributeResolver
{
    /**
     * @param ReflectionMethod $method
     * @param string $availableTag
     * @param string $version
     * @return RouteObject|null
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function __invoke(
        ReflectionMethod $method,
        string $availableTag,
        string $version
    ): ?RouteObject {

        /** @var Action $action */
        $action = $this->resolveRouteActionAnnotation($method);

        if ($action == null) {
            return null;
        }

        if (!$this->isAvailable($action, $availableTag, $version)) {
            return null;
        }

        /** @var Route|null $route */
        $route = $this->resolveRouteAnnotation($method);

        if ($route == null) {
            return null;
        }

        $methods = $route->getMethods();
        return $action == null
            ? null
            : new RouteObject($route->getPath(), strtolower(end($methods)), $action);
    }

    #[Pure]
    private function isAvailable(Action $routeAction, string $tagAvailable, string $version): bool
    {
        if ($tagAvailable === 'internal') {
            return true;
        }
        if (!empty($routeAction->getAvailableTags()) && !in_array($tagAvailable, $routeAction->getAvailableTags())) {
            return false;
        }

        if ($routeAction->getVersion() !== null && $routeAction->getVersion() !== $version) {
            return false;
        }

        return in_array($tagAvailable, $routeAction->getAvailableTags());
    }

    /**
     * @param ReflectionMethod $method
     * @return object|null
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function resolveRouteAnnotation(ReflectionMethod $method): ?object
    {
        $attribute = $this->resolveAttribute($method, Route::class);
        if ($attribute === null) {
            return null;
        }
        return $attribute->newInstance();
    }

    /**
     * @param ReflectionMethod $method
     * @return object|null
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function resolveRouteActionAnnotation(ReflectionMethod $method): ?object
    {
        $attribute = $this->resolveAttribute($method, Action::class);
        if ($attribute === null) {
            return null;
        }
        return $attribute->newInstance();
    }
}