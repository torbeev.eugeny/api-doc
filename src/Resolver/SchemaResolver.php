<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver;

use EAP\Packages\ApiDoc\Annotation\Property;
use EAP\Packages\ApiDoc\DTO\Object\SchemaObject;
use EAP\Packages\ApiDoc\DTO\Swagger\Content\ContentType;
use MyCLabs\Enum\Enum;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\{SchemaPropertyObjectDefinition, SchemaPropertyArrayDefinition};
use ReflectionClass;
use EAP\Packages\ApiDoc\DTO\Object\SchemaCollection;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaProperty;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\Schema;
use EAP\Packages\HttpMapper\Resolver\ClassConstructorValuesResolver;
use EAP\Packages\HttpMapper\Resolver\Traits\ArgumentsResolverTrait;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Uid\Uuid;
use DateTime;
use DateTimeImmutable;
use ReflectionException;
use ReflectionProperty;
use ReflectionAttribute;
use Exception;
use Psr\SimpleCache\InvalidArgumentException;

class SchemaResolver
{
    use ArgumentsResolverTrait;

    private const SUPPORTED_CLASSES = [Uuid::class, DateTimeImmutable::class, DateTime::class];

    private ClassConstructorValuesResolver $constructorValuesResolver;
    private ClassPropertyResolveHandler $propertyResolveHandler;
    private SchemaCollection $schemaCollection;
    private NormalizerInterface $normalizer;

    public function __construct(
        ClassConstructorValuesResolver $constructorValuesResolver,
        SchemaCollection $schemaCollection,
        ClassPropertyResolveHandler $propertyResolveHandler,
        NormalizerInterface $normalizer
    ) {
        $this->constructorValuesResolver = $constructorValuesResolver;
        $this->schemaCollection = $schemaCollection;
        $this->propertyResolveHandler = $propertyResolveHandler;
        $this->normalizer = $normalizer;
    }

    /**
     * @param string $schemaClass
     * @param bool $isQuery
     * @return object
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function __invoke(string $schemaClass, bool $isQuery): object
    {
        return $this->resolveArguments($schemaClass, $isQuery);
    }

    /**
     * @param string $class
     * @param bool $isQuery
     * @param bool $collection
     * @param bool $isMain
     * @return SchemaCollection|SchemaObject
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function resolveArguments(
        string $class,
        bool $isQuery,
        bool $collection = true,
        bool $isMain = true
    ): SchemaCollection|SchemaObject {
        var_dump($class);
        $schema = new SchemaObject($this->definitionName($class), new Schema(), $isMain);
        $reflection = new ReflectionClass($class);

        do {
            $this->resolveClassArguments($reflection, $schema, $isQuery);
        } while($reflection = $reflection->getParentClass());

        $this->schemaCollection->add($schema);

        return $collection ? $this->schemaCollection : $schema;
    }

    /**
     * @param ReflectionClass $reflection
     * @param SchemaObject $schema
     * @param bool $isQuery
     * @return void
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function resolveClassArguments(ReflectionClass &$reflection, SchemaObject &$schema, bool $isQuery): void
    {
        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);
            /** @var SchemaProperty|SchemaPropertyObjectDefinition $schemaProperty */
            $schemaProperty = $this->resolveClassProperty($reflection, $property, $isQuery);
            if ($schemaProperty == null) {
                continue;
            }
            if ($schemaProperty instanceof SchemaProperty && $schemaProperty->getFormat() == 'binary') {
                $schema->setIsUsable(false);
                $schema->setContentType(ContentType::multipart());
                $schema->addProperty($property->getName(), $schemaProperty);
            }
            if ($isQuery) {
                $schema->setIsQuery(true);
            }
            $serializeName = $this->resolvePropertyAttribute($property, SerializedName::class, true);
            $propertyName = $serializeName == null ? $property->getName() : $serializeName;
            $schema->addProperty($propertyName, $schemaProperty);
            $property->setAccessible(false);
        }
    }

    /**
     * @param ReflectionClass $reflection
     * @param ReflectionProperty $property
     * @param bool $isQuery
     * @return object|null
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function resolveClassProperty(ReflectionClass &$reflection, \ReflectionProperty &$property, bool $isQuery): ?object
    {
        $type = str_replace('?', '', (string)$property->getType());

        if (count($property->getAttributes(Property::class)) > 0) {
            $arguments = $property->getAttributes()[0]->getArguments();
            if (array_key_exists('type', $arguments) && $arguments['type'] === 'ignore') {
                return null;
            }
            return $this->resolveProperty($property);
        }

        if ($type === 'array') {
            return $this->resolveArrayType($reflection, $property, $isQuery);
        }

        if ($this->isClass($type) && !$this->propertyResolveHandler->isSupported($property) ) {
            return $this->resolveTypeAsClass($property, $isQuery);
        }

        return $this->resolveProperty($property);
    }

    /**
     * @param ReflectionClass $reflectionClass
     * @param ReflectionProperty $property
     * @param bool $isQuery
     * @return SchemaPropertyArrayDefinition|null
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function resolveArrayType(
        ReflectionClass &$reflectionClass,
        \ReflectionProperty &$property,
        bool $isQuery
    ): ?SchemaPropertyArrayDefinition {
        $statements = $this->getUseStatements($reflectionClass);
        if (!is_string($property->getDocComment())) {
            return null;
        }

        preg_match("/var(.*)\[]/", $property->getDocComment(), $matches );
        if (!$matches) {
            return null;
        }

        $className = trim(end($matches));
        $class = $statements[$className] ?? $this->identifyFullClassName($reflectionClass->getName(), $className);

        $this->resolveArguments($class, $isQuery,false, false);

        return new SchemaPropertyArrayDefinition(new SchemaPropertyObjectDefinition($className));
    }

    /**
     * @param ReflectionProperty $property
     * @return object|null
     * @throws InvalidArgumentException
     * @throws ReflectionException
     * @throws Exception
     */
    private function resolveProperty(\ReflectionProperty &$property): ?SchemaProperty
    {
        /** @var ReflectionAttribute|null $attribute */
        $attribute = $this->resolvePropertyAttribute($property, Property::class);
        $arguments = $attribute === null ? ($this->propertyResolveHandler)($property) : $attribute->getArguments();

        if (!is_array($arguments)) {
            $arguments = $arguments->toArray();
        }

        return new SchemaProperty(
            $arguments['type'],
            isset($arguments['format']) ? (string)$arguments['format'] : null,
            $arguments['example'] ?? null,
            $arguments['required'] ?? false
        );
    }

    /**
     * @param ReflectionProperty $property
     * @param bool $isQuery
     * @return SchemaPropertyObjectDefinition
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function resolveTypeAsClass(\ReflectionProperty &$property, bool $isQuery): SchemaPropertyObjectDefinition
    {
        $class = $property->getType()->getName();
        $reflection = new ReflectionClass($class);
        $schema = new SchemaObject($this->definitionName($class), new Schema(), false);
        $this->resolveClassArguments($reflection, $schema, $isQuery);
        $this->schemaCollection->add($schema);

        return new SchemaPropertyObjectDefinition($this->definitionName($class));
    }

    /**
     * @param ReflectionProperty $property
     * @param string $attribute
     * @param bool $argument
     * @return ReflectionAttribute|string|null
     */
    private function resolvePropertyAttribute(
        \ReflectionProperty $property,
        string $attribute,
        bool $argument = false
    ): ReflectionAttribute|string|null {
        $attributes = $property->getAttributes($attribute);
        if (empty($attributes)) {
            return null;
        }

        if ($argument) {
            /** @var ReflectionAttribute $attribute */
            $attribute = end($attributes);
            $arguments = $attribute->getArguments();
            return end($arguments);
        }

        return end($attributes);
    }

    /**
     * @param string $existClass
     * @param string $lookClass
     * @return string|null
     */
    private function identifyFullClassName(string $existClass, string $lookClass):? string
    {
        $path = explode('\\', $existClass);
        array_pop($path);
        array_push($path, $lookClass);
        $class = '';

        foreach ($path as $part) {
            $class .= "{$part}\\";
        }

        $classPath = substr($class, 0, -1);

        return class_exists($classPath) ? $classPath : null;
    }

    /**
     * @param string $classPath
     * @return string
     */
    private function definitionName(string $classPath): string
    {
        $path = explode('\\', $classPath);
        return end($path);
    }
}