<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver;

use EAP\Packages\ApiDoc\Annotation\Identify;
use EAP\Packages\ApiDoc\Annotation\Response;
use EAP\Packages\ApiDoc\DTO\Object\RouteResponseObject;
use EAP\Packages\ApiDoc\DTO\Object\SchemaCollection;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaComponentWrapped;
use EAP\Packages\ApiDoc\DTO\Swagger\Content\RouteContent;
use EAP\Packages\ApiDoc\DTO\Swagger\Content\RouteContentJson;
use EAP\Packages\ApiDoc\DTO\Swagger\Route\RouteResponse;
use EAP\Packages\ApiDoc\Exception\MissedAttributeException;
use EAP\Packages\HttpMapper\Http\Response\BadRequestResponse;
use EAP\Packages\HttpMapper\Http\Response\NotFoundResponse;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;
use ReflectionMethod;
use ReflectionAttribute;
use Exception;

class RouteResponseResolver extends RouteAttributeResolver
{
    private const METHODS_DEFAULT_RESPONSE_AVAILABLE = ['POST', 'PUT', 'PATCH'];
    private const DEFAULT_ROUTES = [
        NotFoundResponse::HTTP_STATUS_CODE => ['message' => NotFoundResponse::MESSAGE],
        BadRequestResponse::HTTP_STATUS_CODE => ['message' => BadRequestResponse::MESSAGE]
    ];

    /**
     * @param ReflectionMethod $method
     * @param bool $addDefaultResponses
     * @param string $action
     * @param bool $wrapResponseSchema
     * @return array
     * @throws InvalidArgumentException
     * @throws ReflectionException
     * @throws Exception
     */
    public function __invoke(
        ReflectionMethod $method,
        bool $addDefaultResponses,
        string $action,
        bool $wrapResponseSchema = true
    ): array {
        $routes = array_map(
            fn(ReflectionAttribute $attribute) => $this->generateResponseObjectFromAttribute($attribute, $wrapResponseSchema),
            $method->getAttributes(Response::class)
        );

        $identify = $method->getAttributes(Identify::class) != null;
        if (
            $addDefaultResponses
            && in_array(strtoupper($action), self::METHODS_DEFAULT_RESPONSE_AVAILABLE)
            || $identify
        ) {
            $this->addDefaultResponses($routes, $wrapResponseSchema, $identify);
        }

        return $routes;
    }

    /**
     * @param array $routes
     * @param bool $wrapResponseSchema
     * @param bool $hasIdentify
     * @throws Exception
     */
    private function addDefaultResponses(array &$routes, bool $wrapResponseSchema, bool $hasIdentify = false): void
    {
        foreach (self::DEFAULT_ROUTES as $httpCode => $responseDetails) {
            if (!isset($responseDetails['message']) && !isset($responseDetails['schema'])) {
                throw new Exception('Not valid default route attributes');
            }
            if ($httpCode === 404 && !$hasIdentify) {
                continue;
            } else {
                $schema = SchemaComponentWrapped::wrappedErrorSchema($httpCode, $responseDetails['message']);
                $content = new RouteContent($schema);
                $response = new RouteResponse('Error response', new RouteContentJson($content));
                array_push($routes, new RouteResponseObject($response, $httpCode));
            }

        }
    }

    /**
     * @param ReflectionAttribute $attribute
     * @param bool $wrapResponseSchema
     * @return RouteResponseObject
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function generateResponseObjectFromAttribute(
        ReflectionAttribute $attribute,
        bool $wrapResponseSchema
    ): RouteResponseObject
    {
        $arguments = $attribute->getArguments();

        if (!isset($arguments['httpCode'])) {
            throw new MissedAttributeException('Route response can not be without http code');
        }

        if (isset($arguments['schema'])) {
            /** @var SchemaCollection  $schemaCollection */
            $schemaCollection = ($this->schemaResolver)($arguments['schema'], false);

            $schemaName = $this->getDefinitionName($arguments['schema']);
            $responseSchema = $wrapResponseSchema
                ? RouteResponse::createWithSchemaWrapped($schemaName, $schemaName)
                : new RouteResponse($schemaName, new RouteContentJson($schemaName));

            return new RouteResponseObject(
                $responseSchema,
                $arguments['httpCode'],
                $schemaCollection
            );
        }

        return new RouteResponseObject(
            new RouteResponse('Response object'),
            $arguments['httpCode']
        );
    }
}