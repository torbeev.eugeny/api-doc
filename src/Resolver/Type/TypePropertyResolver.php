<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver\Type;

use EAP\Packages\ApiDoc\DTO\Object\PropertyAttribute;
use ReflectionProperty;

interface TypePropertyResolver
{
    public function supports(string $type): bool;
    public function __invoke(ReflectionProperty &$property): object;
}