<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver\Type;

use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaProperty;
use JetBrains\PhpStorm\Pure;
use ReflectionProperty;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class UploadFileTypeResolver implements TypePropertyResolver
{


    public function __invoke(ReflectionProperty &$property): object
    {
        return new SchemaProperty('string', 'binary');
    }

    public function supports(string $type): bool
    {
        return is_a($type, UploadedFile::class, true);
    }
}