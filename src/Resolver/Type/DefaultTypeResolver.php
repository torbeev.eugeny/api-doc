<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver\Type;

use EAP\Packages\ApiDoc\DTO\Object\PropertyAttribute;
use EAP\Packages\HttpMapper\Resolver\Traits\ArgumentsResolverTrait;
use MyCLabs\Enum\Enum;
use ReflectionProperty;
use Symfony\Component\Uid\Uuid;
use Exception;
use DateTime;
use DateTimeImmutable;

final class DefaultTypeResolver implements TypePropertyResolver
{
    use ArgumentsResolverTrait;

    private const SUPPORTED_TYPES = [Uuid::class, DateTimeImmutable::class, DateTime::class, 'int', 'string', 'bool'];

    public function supports(string $type): bool
    {
        return in_array($type, self::SUPPORTED_TYPES, true) || is_subclass_of($type, Enum::class);
    }

    /**
     * @param ReflectionProperty $property
     * @return object
     * @throws Exception
     */
    public function __invoke(ReflectionProperty &$property): object
    {
        $name = $this->defineSerializedName($property);

        $type = $this->getPropertyType($property);
        return match ($type) {
            Uuid::class => PropertyAttribute::uuidType($name),
            DateTimeImmutable::class, DateTime::class => PropertyAttribute::datetimeType($name),
            'int' => PropertyAttribute::intType($name),
            'string' => PropertyAttribute::stringType($name),
            'bool' => PropertyAttribute::booleanType($name),
            default => match (true) {
                is_subclass_of($type, Enum::class) => PropertyAttribute::enumType($name, $type::toArray()),
                default => throw new Exception("Undefined type {$this->getPropertyType($property)}"),
            },
        };
    }
}