<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Resolver;

use EAP\Packages\HttpMapper\Resolver\ClassConstructorValuesResolver;
use ReflectionMethod;
use ReflectionProperty;
use ReflectionAttribute;

class RouteAttributeResolver
{
    protected SchemaResolver $schemaResolver;
    protected ClassConstructorValuesResolver $constructorValuesResolver;

    public function __construct(
        SchemaResolver $schemaResolver,
        ClassConstructorValuesResolver $constructorValuesResolver
    ) {
        $this->schemaResolver = $schemaResolver;
        $this->constructorValuesResolver = $constructorValuesResolver;
    }

    protected function resolveAttribute(
        ReflectionMethod|ReflectionProperty $reflection,
        string $attribute
    ): ?ReflectionAttribute {
        $attributes = $reflection->getAttributes($attribute);
        if (empty($attributes)) {
            return null;
        }

        return end($attributes);
    }

    protected function getDefinitionName(string $class): string
    {
        $path = explode('\\', $class);
        return end($path);
    }
}