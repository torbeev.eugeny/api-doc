<?php


namespace EAP\Packages\ApiDoc\Annotation;

use Attribute;
use EAP\Packages\ApiDoc\Annotation\Contracts\TypeContract;

/**
 * @Annotation
 * @NamedAnnotationConstructor()
 * @Target({"PROPERTY","ANNOTATION"})
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Identify implements TypeContract
{
    public string $in = 'path';
    public string $name = 'id';
    public string $type = 'string';
    public string $format = 'uuid';
    public string $description = 'ID';
    public bool $required = true;
    public string $example = '0f421cf0-aeb6-420c-b23a-d7a80653f502';

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getExample(): string
    {
        return $this->example;
    }
}