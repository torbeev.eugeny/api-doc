<?php

namespace EAP\Packages\ApiDoc\Annotation;

/**
 * Annotation that indicates that the annotated class should be constructed with a named argument call.
 *
 * @Annotation
 * @Target("CLASS")
 */
final class NamedAnnotationConstructor
{
}
