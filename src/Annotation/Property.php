<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Annotation;

use Attribute;

/**
 * @Annotation
 * @NamedAnnotationConstructor()
 * @Target({"PROPERTY","ANNOTATION"})
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
final class Property implements Annotation
{
    public string $type;

    public mixed $example;

    public ?string $description;

    public ?string $format;

    public bool $required;

    public function __construct(
        string $type,
        mixed $example = null,
        ?string $description = null,
        ?string $format = null,
        bool $required = false
    ) {
        $this->type = $type;
        $this->example = $example;
        $this->description = $description;
        $this->format = $format;
        $this->required = $required;
    }
}
