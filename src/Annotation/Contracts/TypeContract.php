<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Annotation\Contracts;

interface TypeContract
{
    public function getName(): string;
    public function getType(): string;
    public function getFormat(): string;
    public function getDescription(): string;
    public function getExample(): string;
}