<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Annotation;

use Attribute;

/**
 * @Annotation
 * @NamedAnnotationConstructor()
 * @Target({"PROPERTY","ANNOTATION"})
 */
#[Attribute(Attribute::TARGET_METHOD)]
final class Action implements Annotation
{
    public array $tags;

    public string $summary;

    public ?string $description = null;

    public bool $secured = false;

    public ?string $version = null;

    public array $availableTags = [];

    public function __construct(
        array $tags,
        string $summary,
        string $description = null,
        bool $secured = false,
        string $version = null,
        array $availableTags = []
    ) {
        $this->tags = $tags;
        $this->summary = $summary;
        $this->description = $description;
        $this->version = $version;
        $this->availableTags = $availableTags;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function getSummary(): string
    {
        return $this->summary;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function isSecured(): bool
    {
        return $this->secured;
    }

    public function setSecured(bool $secured): void
    {
        $this->secured = $secured;
    }

    public function getAvailableTags(): array
    {
        return $this->availableTags;
    }

    public function setAvailableTags(array $availableTags): void
    {
        $this->availableTags = $availableTags;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }
}
