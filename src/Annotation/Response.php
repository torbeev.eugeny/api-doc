<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Annotation;

use Attribute;

/**
 * @Annotation
 * @NamedAnnotationConstructor()
 * @Target({"PROPERTY","ANNOTATION"})
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class Response
{
    public int $httpCode;
    public ?string $schema;
    public ?string $description;

    /**
     * Response constructor.
     * @param int $httpCode
     * @param string|null $schema
     * @param string|null $description
     */
    public function __construct(int $httpCode, ?string $schema = null, ?string $description = null)
    {
        $this->httpCode = $httpCode;
        $this->schema = $schema;
        $this->description = $description;
    }
}