<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc;

use JetBrains\PhpStorm\Pure;
use ReflectionMethod;
use ReflectionClass;
use EAP\Packages\ApiDoc\Annotation\Action;
use EAP\Packages\ApiDoc\Annotation\Response;
use EAP\Packages\ApiDoc\DTO\Object\RouteObject;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SecuritySchema;
use EAP\Packages\ApiDoc\DTO\Object\SchemaObject;
use EAP\Packages\ApiDoc\Resolver\RouteObjectResolver;
use EAP\Packages\ApiDoc\Controller\UiController;
use EAP\Packages\ApiDoc\DTO\Swagger\Route\RoutePath;
use EAP\Packages\ApiDoc\DTO\Swagger\SwaggerDoc;
use EAP\Packages\ApiDoc\Resolver\RouteParameterResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ReflectionException;
use Symfony\Component\Routing\Annotation\Route;
use EAP\Packages\ApiDoc\Resolver\RouteResponseResolver;
use InvalidArgumentException;

final class ApiDocBuilder
{
    private const IGNORE_CONTROLLERS = ['error_controller::preview', UiController::class];
    private const IGNORE_ATTRIBUTES = [Route::class, Action::class, Response::class];

    private ContainerInterface $container;
    private RouteParameterResolver $parameterResolver;
    private RouteObjectResolver $routeObjectResolver;
    private RouteResponseResolver $responseResolver;
    private ApiDocConfigure $configure;

    public function __construct(
        ContainerInterface $container,
        RouteParameterResolver $parameterResolver,
        RouteObjectResolver $routeObjectResolver,
        RouteResponseResolver $responseResolver,
        ApiDocConfigure $configure
    ) {
        $this->container = $container;
        $this->parameterResolver = $parameterResolver;
        $this->routeObjectResolver = $routeObjectResolver;
        $this->responseResolver = $responseResolver;
        $this->configure = $configure;
    }

    /**
     * @param string $availableTag
     * @param string $version
     * @return SwaggerDoc
     * @throws ReflectionException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function __invoke(string $availableTag, string $version): SwaggerDoc
    {
        $doc = new SwaggerDoc();
        $doc->setTitle($this->configure->getTitle());
        $doc->setDescription($this->configure->getDescription());
        $doc->setVersion($this->configure->getVersion());
        $doc->setServerHost($this->configure->getServerUrl());

        foreach ($this->container->get('router')->getRouteCollection()->all() as $route) {
            $controller = $route->getDefault('_controller');

            if (in_array($controller, self::IGNORE_CONTROLLERS)) {
                continue;
            }

            $this->buildControllerDoc($doc, $controller, $availableTag, $version);
        }

        return $doc;
    }

    /**
     * @param SwaggerDoc $doc
     * @param string $controller
     * @param string $tagAvailable
     * @param string $version
     * @throws ReflectionException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function buildControllerDoc(
        SwaggerDoc &$doc,
        string $controller,
        string $tagAvailable,
        string $version
    ): void {
        $reflection = new ReflectionClass($controller);
        $method = $reflection->getMethod('__invoke');

        $route = ($this->routeObjectResolver)($method, $tagAvailable, $version);

        if ($route !== null) {
            $routeDoc = new RoutePath();
            $routeDoc->setTags($route->getAction()->getTags());
            $routeDoc->setSummary($route->getAction()->getSummary());
            $routeDoc->setDescription($route->getAction()->getDescription());

            $this->appendParametersFromArguments($doc, $routeDoc, $method, $route);
            $this->addResponses($doc, $routeDoc, $method, $route);

            if ($route->getAction()->isSecured()) {
                $routeDoc->addSecurity('Authorization', []);
            }

            $doc->addPath($route->getPath(), $route->getMethod(), $routeDoc);

            $this->addSecuredSchemas($doc);
        }
    }

    /**
     * @param SwaggerDoc $doc
     * @param RoutePath $routePath
     * @param ReflectionMethod $method
     * @param RouteObject $route
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function addResponses(
        SwaggerDoc &$doc,
        RoutePath &$routePath,
        ReflectionMethod $method,
        RouteObject &$route
    ): void {
        $routeRespObjets = ($this->responseResolver)($method, true, $route->getMethod());

        foreach ($routeRespObjets as $routeRespObj) {
            $schemaCollection = $routeRespObj->getSchemaCollection();
            if ($schemaCollection != null) {
                if ($schemaCollection->getMain() !== null) {
                    $routePath->addResponse($routeRespObj->getHttpCode(), $routeRespObj->getResponse());
                    $doc->addSchema(
                        $schemaCollection->getMain()->getName(),
                        $schemaCollection->getMain()->getSchema()
                    );

                    /** @var SchemaObject $schema */
                    foreach ($schemaCollection as $schema) {
                        $doc->addSchema($schema->getName(), $schema->getSchema());
                    }

                    $schemaCollection->erase();
                }
            } else {
                $routePath->addResponse($routeRespObj->getHttpCode(), $routeRespObj->getResponse());
            }
        }
    }

    /**
     * @param SwaggerDoc $doc
     * @param RoutePath $routePath
     * @param ReflectionMethod $method
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    private function appendParametersFromArguments(
        SwaggerDoc &$doc,
        RoutePath &$routePath,
        ReflectionMethod $method,
        RouteObject $route,
    ): void {
        foreach ($method->getParameters() as $parameter) {
            $paramObject = ($this->parameterResolver)($parameter, $route->isQuery());
            if (empty($paramObject->getRouteParameters()) && $paramObject->getRequestBody() == null) {
                continue;
            }

            if ($paramObject->getRequestBody()) {
                $routePath->setRequestBody($paramObject->getRequestBody());
            }

            foreach ($paramObject->getRouteParameters() as $routeParameter) {
                $routePath->addParameter($routeParameter);
            }

            $schemaCollection = $paramObject->getSchemaCollection();
            if ($schemaCollection == null) {
                continue;
            }

            $mainSchema = $schemaCollection->getMain();
            if ($mainSchema == null) {
                continue;
            }
            if ($mainSchema->isUsable() && !$mainSchema->isQuery()) {
                $doc->addSchema($schemaCollection->getMain()->getName(), $schemaCollection->getMain()->getSchema());
            }

            foreach ($schemaCollection as $schema) {
                if (!$schema->isQuery() && $schema->isUsable()) {
                    $doc->addSchema($schema->getName(), $schema->getSchema());
                }
            }

            $schemaCollection->erase();
        }
    }

    private function addSecuredSchemas(SwaggerDoc &$doc): void
    {
        $doc->addSecurityScheme('Authorization', new SecuritySchema('http', 'bearer'));
    }
}