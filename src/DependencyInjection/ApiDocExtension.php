<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DependencyInjection;

use EAP\Packages\ApiDoc\Resolver\Type\TypePropertyResolver;
use Exception;
use EAP\Packages\HttpMapper\DependencyInjection\Configuration;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

class ApiDocExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $containerBuilder
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $containerBuilder): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new PhpFileLoader($containerBuilder, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('config.php');

        $containerBuilder->registerForAutoconfiguration(TypePropertyResolver::class)
            ->addTag('api_doc.resolver');
    }
}