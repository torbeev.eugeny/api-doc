<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('eap_api_doc');
        $treeBuilder->getRootNode()->children()->arrayNode('schema_dirs')
            ->useAttributeAsKey('namespace')
            ->scalarPrototype()
            ->end()
            ->end()
            ->end(); // @phpstan-ignore-line
        return $treeBuilder;
    }
}