<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DependencyInjection\Compiler;

use EAP\Packages\ApiDoc\Resolver\Type\DefaultTypeResolver;
use EAP\Packages\ApiDoc\Resolver\Type\MoneyTypeResolver;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ResolverTagCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(ResolverChain::class)) {
            return;
        }

        $definition = $container->findDefinition(ResolverChain::class);
        $taggedServices = $container->findTaggedServiceIds('api_doc.resolver');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                if (!empty($tag) && isset($tag['type'])) {
                    $definition->addMethodCall('addResolver', [new Reference($id), $tag['type']]);
                }
            }
        }
    }
}
