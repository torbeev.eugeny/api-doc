<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DependencyInjection\Compiler;

use EAP\Packages\ApiDoc\Resolver\Type\TypePropertyResolver;

class ResolverChain
{
    private array $resolvers = [];

    public function __construct()
    {
    }

    public function addResolver(TypePropertyResolver $resolver, string $type): void
    {
        $this->resolvers[$type] = $resolver;
    }

    public function getResolver(string $type): ?TypePropertyResolver
    {
        return $this->resolvers[$type] ?? null;
    }
}