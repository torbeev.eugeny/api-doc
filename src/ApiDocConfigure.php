<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc;

class ApiDocConfigure
{
    private string $title;
    private string $description;
    private string $version;
    private string $serverUrl;

    public function __construct(string $title, string $description, string $version, string $serverUrl)
    {
        $this->title = $title;
        $this->description = $description;
        $this->version = $version;
        $this->serverUrl = $serverUrl;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getServerUrl(): string
    {
        return $this->serverUrl;
    }
}
