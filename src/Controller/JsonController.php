<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Controller;

use EAP\Packages\ApiDoc\ApiDocBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

final class JsonController
{
    private SerializerInterface $serializer;
    private ApiDocBuilder $builder;

    public function __construct(SerializerInterface $serializer, ApiDocBuilder $builder)
    {
        $this->serializer = $serializer;
        $this->builder = $builder;
    }

    /**
     * @param $tag
     * @param $version
     * @return JsonResponse
     * @throws \ReflectionException
     */
    public function __invoke($tag, $version): JsonResponse
    {
        $content = $this->serializer->serialize(($this->builder)($tag, $version), 'json', [
            ObjectNormalizer::SKIP_NULL_VALUES => true
        ]);

        return new JsonResponse($content, 200, [], true);
    }
}
