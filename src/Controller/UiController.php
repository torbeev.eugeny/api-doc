<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class UiController
{
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }


    public function __invoke($tag): Response
    {
        return $this->returnUi($tag);
    }

    private function returnUi(string $tag): Response
    {
        $url = $this->urlGenerator->generate('eap_api_doc_json', ['version' => '1v', 'tag' => $tag]);
        $baseUrl = 'https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.36.2/';
        return new Response('<html>
            <head>
            <meta charset="UTF-8">
            <link href="' . $baseUrl . 'swagger-ui.css" rel="stylesheet">
            </head>
            <body>
            <div id="swagger-ui"></div>
            <script src="' . $baseUrl . 'swagger-ui-bundle.js"></script>
            <script src="' . $baseUrl . 'swagger-ui-standalone-preset.js"></script>
            <script>
                window.onload = function() {
                  const ui = SwaggerUIBundle({
                    "dom_id": "#swagger-ui",
                    deepLinking: true,
                    presets: [
                      SwaggerUIBundle.presets.apis,
                      SwaggerUIStandalonePreset
                    ],
                    plugins: [
                      SwaggerUIBundle.plugins.DownloadUrl
                    ],
                    layout: "StandaloneLayout",
                    url: "' . $url . '",
                  });
                  window.ui = ui;
                }
            </script>
            </body>
            </html>
        ', 200, ['Content-Type' => 'text/html']);
    }
}