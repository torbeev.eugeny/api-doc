<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Content;

use EAP\Packages\ApiDoc\DTO\Swagger\Component\{SchemaComponentObject,
    SchemaPropertyObjectDefinition,
    SchemaComponentWrapped};

class RouteContent
{
    private SchemaPropertyObjectDefinition|SchemaComponentObject $schema;

    public function __construct(SchemaPropertyObjectDefinition|SchemaComponentObject $schema)
    {
        $this->schema = $schema;
    }

    public function getSchema(): SchemaPropertyObjectDefinition|SchemaComponentObject
    {
        return $this->schema;
    }

    public static function wrapSchemaContent(string $definitionName): self
    {
        return new self(SchemaComponentWrapped::wrapDefinition($definitionName));
    }
}
