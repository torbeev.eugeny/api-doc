<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Content;

use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaComponentObject;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\SerializedName;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaPropertyObjectDefinition;

class RouteContentJson implements RouteContentType
{
    #[SerializedName('application/json')]
    private RouteContent $formatDefinition;

    #[Pure]
    public function __construct(string|RouteContent|SchemaComponentObject $definition)
    {
        if (is_string($definition)) {
            $this->formatDefinition = new RouteContent(new SchemaPropertyObjectDefinition($definition));
        }

        if ($definition instanceof SchemaComponentObject) {
            $this->formatDefinition = new RouteContent($definition);
        }

        if ($definition instanceof RouteContent) {
            $this->formatDefinition = $definition;
        }
    }

    public function getFormatDefinition(): RouteContent
    {
        return $this->formatDefinition;
    }
}
