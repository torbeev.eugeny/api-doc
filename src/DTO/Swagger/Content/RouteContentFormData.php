<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Content;

use EAP\Packages\ApiDoc\DTO\Swagger\Component\Schema;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\SerializedName;

class RouteContentFormData implements RouteContentType
{
    #[SerializedName('multipart/form-data')]
    private RouteContent $formatDefinition;

    #[Pure]
    public function __construct(Schema $schema)
    {
        $this->formatDefinition = new RouteContent($schema);
    }

    public function getFormatDefinition(): RouteContent
    {
        return $this->formatDefinition;
    }
}