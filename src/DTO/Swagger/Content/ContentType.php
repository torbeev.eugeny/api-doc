<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Content;

use JetBrains\PhpStorm\Pure;

class ContentType
{
    public const JSON = 'application/json';
    public const MULTI_PART_FORM_DATA = 'multipart/form-data';

    private string $type;

    private function __construct(string $type)
    {
        $this->type = $type;
    }

    #[Pure]
    public static function json(): self
    {
        return new self(self::JSON);
    }

    #[Pure]
    public static function multipart(): self
    {
        return new self(self::MULTI_PART_FORM_DATA);
    }

    public function __toString(): string
    {
        return $this->type;
    }
}