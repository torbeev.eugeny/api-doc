<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Content;

interface RouteContentType
{
    public function getFormatDefinition(): RouteContent;
}