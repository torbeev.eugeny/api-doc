<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger;

use JetBrains\PhpStorm\Pure;

use EAP\Packages\ApiDoc\DTO\Swagger\Component\SwaggerComponents;
use EAP\Packages\ApiDoc\DTO\Swagger\Route\RoutePath;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\{Schema, SecuritySchema};

class SwaggerDoc
{
    /** @var string */
    private string $openapi = '3.0.0';

    /** @var array<string,string> */
    private array $info = [
        'description' => '',
        'version' => '1.0.0',
        'title' => 'Swagger api doc'
    ];

    /** @var array */
    private array $servers = [
        [
        ]
    ];

    /** @var array<string, <string, RoutePath>> */
    private array $paths;

    /** @var SwaggerComponents */
    private SwaggerComponents $components;

    #[Pure]
    public function __construct()
    {
        $this->components = new SwaggerComponents();
    }

    public function setTitle(string $title): void
    {
        $this->info['title'] = $title;
    }

    public function setVersion(string $version): void
    {
        $this->info['version'] = $version;
    }

    public function setDescription(string $description): void
    {
        $this->info['description'] = $description;
    }

    public function setServerHost(string $serverHost): void
    {
        $this->servers[]['url'] = $serverHost;
    }

    public function getOpenapi(): string
    {
        return $this->openapi;
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function getServers(): array
    {
        return $this->servers;
    }

    public function getPaths(): array
    {
        return $this->paths;
    }

    public function addPath(string $path, string $method, RoutePath $route): void
    {
        $this->paths[$path][$method] = $route;
    }

    public function getComponents(): SwaggerComponents
    {
        return $this->components;
    }

    public function addSchema(string $name, Schema $schema): void
    {
        $this->components->addSchema($name, $schema);
    }

    public function addSecurityScheme(string $name, SecuritySchema $schema): void
    {
        $this->components->addSecurityScheme($name, $schema);
    }
}
