<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Component;

class SchemaComponentWrapped implements SchemaComponentObject
{
    private string $type = 'object';
    private array $properties = [
        'success' => ['type' => 'boolean', 'example' => true],
        'status' => ['type' => 'string', 'example' => 'ok'],
        'message' => ['type' => 'string', 'example' => 'Message']
    ];

    public function __construct(
        SchemaPropertyObjectDefinition|null $definition,
        bool $success = true,
        string $status = 'ok',
        string $message = 'Created'
    ) {
        $this->properties['data'] = $definition;
        $this->properties['success']['example'] = $success;
        $this->properties['status']['example'] = $status;
        $this->properties['message']['example'] = $message;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }

    public static function wrapDefinition(string $definitionName): self
    {
        return new self(new SchemaPropertyObjectDefinition($definitionName));
    }

    public static function wrappedErrorSchema(int $httpCode = 500, ?string $message = null, ?string $status = null): self
    {
        switch ($httpCode) {
            case 404:
                $message = $message ?? 'Ресурс не найден';
                $status = $status ?? 'error_not_found';
                break;
            case 400:
                $message = $message ?? 'Некорректные данные в запросе';
                $status = $status ?? 'error_bad_request';
                break;
            case 500:
            default:
                $message = $message ?? 'Внутренняя ошибка сервера';
                $status = $status ?? 'error_internal';
                break;
        }
        return new self(
            null,
            false,
            $status,
            $message
        );
    }
}