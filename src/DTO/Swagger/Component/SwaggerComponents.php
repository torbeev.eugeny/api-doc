<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Component;

class SwaggerComponents
{
    /** @var array<string, Schema> */
    private array $schemas = [];

    /** @var array<string, SecuritySchema>|null */
    private ?array $securitySchemes = null;

    public function addSchema(string $name, Schema $schema): void
    {
        if (!isset($this->schemas[$name])) {
            $this->schemas[$name] = $schema;
        }
    }

    public function getSchemas(): array
    {
        return $this->schemas;
    }

    public function addSecurityScheme(string $name, SecuritySchema $schema): void
    {
        if (!isset($this->securitySchemas[$name])) {
            $this->securitySchemes[$name] = $schema;
        }
    }

    public function getSecuritySchemes(): ?array
    {
        return $this->securitySchemes;
    }
}