<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Component;

interface SchemaComponentObject
{
    /**
     * @return array
     */
    public function getProperties(): array;
}