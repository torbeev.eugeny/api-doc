<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Component;

final class Schema implements SchemaComponentObject
{
    private string $type = 'object';

    /** @var array<string, SchemaPropertyObject|SchemaPropertyObjectDefinition> */
    private array $properties = [];

    public function addProperty(string $name, SchemaPropertyObject|SchemaPropertyObjectDefinition $property): void
    {
        if (!isset($this->properties[$name])) {
            $this->properties[$name] = $property;
        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }
}
