<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Component;

final class SchemaPropertyArrayDefinition implements SchemaPropertyObject
{
    private string $type = 'array';

    private string|SchemaPropertyObjectDefinition $items;

    public function __construct(SchemaPropertyObjectDefinition|string $items)
    {
        $this->items = $items;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getItems(): SchemaPropertyObjectDefinition|string
    {
        return $this->items;
    }
}
