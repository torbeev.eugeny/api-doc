<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Component;

use Symfony\Component\Serializer\Annotation\SerializedName;

final class SchemaPropertyObjectDefinition
{
    #[SerializedName('$ref')]
    private string $ref;

    public function __construct(string $ref)
    {
        $this->ref = "#/components/schemas/{$ref}";
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
}