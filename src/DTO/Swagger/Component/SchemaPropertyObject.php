<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Component;

interface SchemaPropertyObject
{
    public function getType(): string;
}