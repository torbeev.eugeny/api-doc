<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Component;

use JetBrains\PhpStorm\Pure;

class SchemaProperty implements SchemaPropertyObject
{
    private string $type;
    private ?string $format = null;
    private mixed $example = null;
    private bool $required;

    public function __construct(
        string $type,
        ?string $format = null,
        $example = null,
        bool $required = false
    ) {
        $this->type = $type;
        $this->format = $format;
        $this->example = $example;
        $this->required = $required;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function getExample(): mixed
    {
        return $this->example;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'format' => $this->format,
            'example' => $this->example
        ];
    }
}
