<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Route;

use JetBrains\PhpStorm\Pure;
use EAP\Packages\ApiDoc\DTO\Swagger\Content\{RouteContent, RouteContentJson, RouteContentType};

class RouteResponse
{
    private string $description;

    private ?RouteContentType $content;

    public function __construct(string $description, ?RouteContentType $content = null)
    {
        $this->description = $description;
        $this->content = $content;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getContent(): ?RouteContentType
    {
        return $this->content;
    }

    #[Pure]
    public static function createWithSchema(string $description, string $schemaName): self
    {
        return new self($description, new RouteContentJson($schemaName));
    }

    /**
     * @param string $description
     * @param string $schemaName
     * @return static
     */
    public static function createWithSchemaWrapped(string $description, string $schemaName): self
    {
        return new self($description, new RouteContentJson(RouteContent::wrapSchemaContent($schemaName)));
    }
}
