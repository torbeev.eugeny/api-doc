<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Route;

class RouteParameterSchema
{
    private string $type;
    private string $format;
    private ?array $enum = null;

    public function __construct(string $type, string $format, ?array $enum = null)
    {
        $this->type = $type;
        $this->format = $format;
        $this->enum = $enum;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function getEnum(): ?array
    {
        return $this->enum;
    }
}