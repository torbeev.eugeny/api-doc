<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Route;

class RoutePath
{
    /** @var array<int,string> */
    private array $tags = [];

    /** @var string */
    private string $summary;

    /** @var string|null */
    private ?string $description;

    /** @var array<int,RouteParameter> */
    private array $parameters = [];

    /** @var RouteRequestBody|null */
    private ?RouteRequestBody $requestBody = null;

    /** @var array<int,RouteResponse> */
    private array $responses = [];

    /** @var array[] */
    private array $security = [];

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }

    public function addTag(string $tag): void
    {
        if (!in_array($tag, $this->tags)) {
            array_push($this->tags, $tag);
        }
    }

    public function getSummary(): string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): void
    {
        $this->summary = $summary;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function addParameter(RouteParameter $parameter): void
    {
        array_push($this->parameters, $parameter);
    }

    public function getResponses(): array
    {
        return $this->responses;
    }

    public function addResponse(int $httpCode, RouteResponse $response): void
    {
        if (!isset($this->responses[$httpCode])) {
            $this->responses[$httpCode] = $response;
        }
    }

    public function getRequestBody(): ?RouteRequestBody
    {
        return $this->requestBody;
    }

    public function setRequestBody(?RouteRequestBody $requestBody): void
    {
        $this->requestBody = $requestBody;
    }

    public function addSecurity(string $name, array $security): void
    {
        $definition = [];
        $definition[$name] = $security;

        if (!isset($this->security[$name])) {
            $this->security[] = $definition;
        }
    }

    /**
     * @return array<string, string>
     */
    public function getSecurity(): array
    {
        return $this->security;
    }
}
