<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Route;

use JetBrains\PhpStorm\Pure;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaProperty;

class RouteParameter
{
    private const IN_PATH = 'path';
    private const IN_QUERY = 'query';

    private string $in = 'body';

    private string $name;

    private ?string $description = null;

    private bool $required = false;

    private ?SchemaProperty $schema;

    private ?string $example = null;

    public function __construct(
        string $in,
        string $name,
        bool $required,
        ?string $description = null,
        ?SchemaProperty $schema = null,
        ?string $example = null
    ) {
        $this->in = $in;
        $this->name = $name;
        $this->description = $description;
        $this->required = $required;
        $this->schema = $schema;
        $this->example = $example;
    }

    public function getIn(): string
    {
        return $this->in;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getSchema(): ?SchemaProperty
    {
        return $this->schema;
    }

    public function getExample(): ?string
    {
        return $this->example;
    }

    #[Pure]
    public static function inPathWithUuidType(string $name): self
    {
        return new self(
            self::IN_PATH,
            $name,
            true,
            $name,
            new SchemaProperty('string', 'uuid')
        );
    }

    #[Pure]
    public static function inPathStringType(string $name): self
    {
        return new self(
            self::IN_PATH,
            $name,
            true,
            $name,
            new SchemaProperty('string', 'string')
        );
    }

    #[Pure]
    public static function inPathIntType(string $name): self
    {
        return new self(
            self::IN_PATH,
            $name,
            true,
            $name,
            new SchemaProperty('int', 'int64')
        );
    }
}
