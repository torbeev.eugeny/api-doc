<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Swagger\Route;

use EAP\Packages\ApiDoc\DTO\Swagger\Content\RouteContentType;

class RouteRequestBody
{
    private bool $required = true;
    private RouteContentType $content;

    public function __construct(RouteContentType $content)
    {
        $this->content = $content;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getContent(): RouteContentType
    {
        return $this->content;
    }
}
