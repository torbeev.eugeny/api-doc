<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Object;

class RouteAttributeObject
{
    private ?SchemaCollection $schemaCollection;

    public function __construct(SchemaCollection $schemaCollection = null)
    {
        $this->schemaCollection = $schemaCollection;
    }

    public function getSchemaCollection(): ?SchemaCollection
    {
        return $this->schemaCollection;
    }
}
