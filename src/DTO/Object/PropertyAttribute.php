<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Object;

class PropertyAttribute
{
    private string $name;
    private string $type;
    private string $format;
    private ?string $example = null;
    private ?string $description = null;
    private ?array $enum = null;

    public function __construct(
        string $name,
        string $type,
        string $format,
        string $example = null,
        ?string $description = null,
        ?array $enum = null
    ) {
        $this->name = $name;
        $this->type = $type;
        $this->format = $format;
        $this->example = $example;
        $this->description = $description;
        if (is_array($enum)) {
            $this->example = $this->example ?? reset($enum);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function getExample(): ?string
    {
        return $this->example;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getEnum(): ?array
    {
        return $this->enum;
    }

    public static function uuidType(string $name): self
    {
        return new self($name, 'string', 'uuid');
    }

    public static function datetimeType(string $name): self
    {
        return new self($name, 'string', 'date-time');
    }

    public static function intType(string $name): self
    {
        return new self($name, 'integer', 'int64');
    }

    public static function stringType(string $name): self
    {
        return new self($name, 'string', 'string');
    }

    public static function booleanType(string $name): self
    {
        return new self($name, 'boolean', 'boolean');
    }

    public static function enumType(string $name, array $values): self
    {
        return new self($name, 'string', 'string', null, null, $values);
    }

    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'format' => $this->format,
            'example' => $this->example,
            'description' => $this->description,
            'enum' => $this->enum
        ];
    }
}