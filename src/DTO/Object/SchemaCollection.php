<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Object;

use EAP\Packages\ApiDoc\DTO\Swagger\Component\Schema;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaPropertyArrayDefinition;
use Iterator;
use JetBrains\PhpStorm\Pure;

class SchemaCollection implements Iterator, SchemaPriorityCollection, \Countable
{
    /** @var array<string,SchemaObject> */
    private array $items;

    private ?SchemaObject $mainSchema = null;

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function add(SchemaObject $item): void
    {
        if ($item->isMain()) {
            $this->mainSchema = $item;
        }
        if (!isset($this->items[$item->getName()])) {
            $this->items[$item->getName()] = $item;
        }
    }

    public function erase(): void
    {
        $this->items = [];
        $this->mainSchema = null;
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function current()
    {
        return current($this->items);
    }

    public function next()
    {
        return next($this->items);
    }

    public function key(): int|null|string
    {
        return key($this->items);
    }

    #[Pure]
    public function valid(): bool
    {
        return $this->key() !== null;
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function getMain(): SchemaObject|null
    {
        return $this->mainSchema;
    }
}