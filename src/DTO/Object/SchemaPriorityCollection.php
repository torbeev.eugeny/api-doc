<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Object;

interface SchemaPriorityCollection
{
    public function getMain(): SchemaObject|null;
}