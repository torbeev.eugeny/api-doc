<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Object;

use EAP\Packages\ApiDoc\DTO\Swagger\Route\RouteParameter;
use EAP\Packages\ApiDoc\DTO\Swagger\Route\RouteRequestBody;
use JetBrains\PhpStorm\Pure;

class RouteParameterObject extends RouteAttributeObject
{
    /** @var array<int,RouteParameter> */
    private array $routeParameters;

    private ?RouteRequestBody $requestBody;

    #[Pure]
    public function __construct(
        array $routeParameters,
        ?RouteRequestBody $requestBody = null,
        ?SchemaCollection $schemaCollection = null
    ) {
        $this->routeParameters = $routeParameters;
        $this->requestBody = $requestBody;
        parent::__construct($schemaCollection);
    }

    public function getRouteParameters(): array
    {
        return $this->routeParameters;
    }

    public function getRequestBody():RouteRequestBody|null
    {
        return $this->requestBody;
    }
}
