<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Object;

use EAP\Packages\ApiDoc\DTO\Swagger\Route\RouteResponse;
use JetBrains\PhpStorm\Pure;

class RouteResponseObject extends RouteAttributeObject
{
    private RouteResponse $response;
    private int $httpCode;

    #[Pure]
    public function __construct(
        RouteResponse $response,
        int $httpCode,
        ?SchemaCollection $schemaCollection = null
    ) {
        $this->response = $response;
        $this->httpCode = $httpCode;
        parent::__construct($schemaCollection);
    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    public function getResponse(): RouteResponse
    {
        return $this->response;
    }
}
