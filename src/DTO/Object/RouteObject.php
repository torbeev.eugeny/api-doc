<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Object;

use EAP\Packages\ApiDoc\Annotation\Action;
use Symfony\Component\HttpFoundation\Request;

class RouteObject
{
    private string $path;
    private string $method;
    private Action $action;

    /**
     * RouteObject constructor.
     * @param string $path
     * @param string $method
     * @param Action $action
     */
    public function __construct(string $path, string $method, Action $action)
    {
        $this->path = $path;
        $this->method = $method;
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return Action
     */
    public function getAction(): Action
    {
        return $this->action;
    }

    public function isQuery()
    {
        return mb_strtolower($this->method) === mb_strtolower(Request::METHOD_GET);
    }
}