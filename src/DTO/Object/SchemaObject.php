<?php

declare(strict_types=1);

namespace EAP\Packages\ApiDoc\DTO\Object;

use EAP\Packages\ApiDoc\DTO\Swagger\Component\Schema;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaPropertyObject;
use EAP\Packages\ApiDoc\DTO\Swagger\Component\SchemaPropertyObjectDefinition;
use EAP\Packages\ApiDoc\DTO\Swagger\Content\ContentType;
use JetBrains\PhpStorm\Pure;

class SchemaObject
{
    private string $name;
    private bool $isMain;
    private bool $isQuery = false;
    private bool $isUsable = true;
    private ContentType $contentType;
    private Schema $schema;

    #[Pure]
    public function __construct(
        string $name,
        Schema $schema,
        bool $isMain = true,
        bool $isQuery = false
    ) {
        $this->name = $name;
        $this->schema = $schema;
        $this->isMain = $isMain;
        $this->isQuery = $isQuery;
        $this->contentType = ContentType::json();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSchema(): Schema
    {
        return $this->schema;
    }

    public function isMain(): bool
    {
        return $this->isMain;
    }

    public function isUsable(): bool
    {
        return $this->isUsable;
    }

    public function setIsUsable(bool $isUsable): void
    {
        $this->isUsable = $isUsable;
    }

    public function getContentType(): ContentType
    {
        return $this->contentType;
    }

    public function setContentType(ContentType $contentType): void
    {
        $this->contentType = $contentType;
    }

    public function setIsQuery(bool $isQuery): void
    {
        $this->isQuery = $isQuery;
    }

    public function isQuery(): bool
    {
        return $this->isQuery;
    }

    public function addProperty(string $name, SchemaPropertyObject|SchemaPropertyObjectDefinition $property): void
    {
        $this->schema->addProperty($name, $property);
    }
}
